# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.35)
# Database: pwmngr
# Generation Time: 2017-10-13 12:27:49 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table passwords
# ------------------------------------------------------------

DROP TABLE IF EXISTS `passwords`;

CREATE TABLE `passwords` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `notes` text,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `passwords` WRITE;
/*!40000 ALTER TABLE `passwords` DISABLE KEYS */;

INSERT INTO `passwords` (`id`, `uid`, `user_name`, `site_name`, `notes`, `password`)
VALUES
	(1,1,'scott','Facebook','Not real password obv ;)\n','bnVrODJ0NDRsblhnalYyYkhmUGZKdz09'),
	(2,1,'scott1','twitter','Not real password obv ;)\n','bnVrODJ0NDRsblhnalYyYkhmUGZKdz09'),
	(3,1,'scott2','twitter2','Not real password obv ;)\n','bnVrODJ0NDRsblhnalYyYkhmUGZKdz09'),
	(4,1,'scott3','twitter3','Not real password obv ;)\n','bnVrODJ0NDRsblhnalYyYkhmUGZKdz09'),
	(5,1,'scott4','twitter4','Not real password obv ;)\n','bnVrODJ0NDRsblhnalYyYkhmUGZKdz09'),
	(6,1,'scott5','twitter5','Not real password obv ;)\n','bnVrODJ0NDRsblhnalYyYkhmUGZKdz09'),
	(7,1,'scott6','twitter6','Not real password obv ;)\n','bnVrODJ0NDRsblhnalYyYkhmUGZKdz09'),
	(8,1,'twitter','scott1','bnVrODJ0NDRsblhnalYyYkhmUGZKdz09','Not real password obv ;)\n'),
	(9,1,'oijioj','sdasdfjj','jio','UmNEVDJWektpRDBUUmMyM3pXdEtKZz09'),
	(10,1,'oijioj','sdasdfjj','jio','UmNEVDJWektpRDBUUmMyM3pXdEtKZz09');

/*!40000 ALTER TABLE `passwords` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`)
VALUES
	(1,'scott','$2y$10$x9mub8oLvzqgPYfDmpyAFOM6AzVGWwaGTd43JCIuDzx3DRQSF0szC');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
