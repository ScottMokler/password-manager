<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 12/10/2017
 * Time: 09:14
 */


class storage {

    public function __construct()
    {
        // just chucking these in here as time restriction
        $this->database = 'pwmngr';
        $this->dbusername = 'root';
        $this->dbpassword = 'root';

        // connect or die!
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname='. $this->database, $this->dbusername, $this->dbpassword);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function fetchListing() {


        $uid = $_SESSION['uid'];

        $sql = "SELECT * FROM `passwords` WHERE uid = $uid";

        $rows = $this->conn->query($sql);

        return $rows;

    }

    public function getPasswordDetail($uid, $pwid) {

        $uid = filter_var($uid,FILTER_SANITIZE_NUMBER_INT);

        $sql = "SELECT p.id, p.uid, p.site_name, p.notes, p.password as `password`, p.user_name FROM passwords p 
        LEFT JOIN users u ON p.uid = u.id
        WHERE p.uid=$uid AND p.id=$pwid AND u.username = '" . $_SESSION['username'] . "'";

        $rows = $this->conn->query($sql);

        if($rows->rowCount()==0) {
            return false;
        }

        return $rows;
    }

    public function delete($pwid) {

        $sql = "DELETE FROM passwords WHERE id=:pwid";
        $statement = $this->conn->prepare($sql);
        $statement->bindParam(':pwid', $pwid, PDO::PARAM_INT);

        try {
            $deleted = $statement->execute();
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        if($deleted) {
            return true;
        }else{
            return false;
        }

    }

    public function saveDetails($post) {

        $sitename = filter_var($post['sitename'],FILTER_SANITIZE_STRING);
        $username = filter_var($post['user_name'],FILTER_SANITIZE_STRING);
        $password = $this->encrypt($post['password'], 'e');
        $notes    = filter_var($post['notes'],FILTER_SANITIZE_STRING);
        $uid      = $_SESSION['uid'];

        $sql = "INSERT INTO `passwords` (`uid`, `site_name`, `user_name`, `password`, `notes`) VALUES (:uid, :site_name, :user_name, :password, :notes)";

        $statement = $this->conn->prepare($sql);

        $statement->bindValue(':uid', $uid);
        $statement->bindValue(':site_name', $sitename);
        $statement->bindValue(':user_name', $username);
        $statement->bindValue(':password', $password);
        $statement->bindValue(':notes', $notes);

        try {
            $inserted = $statement->execute();
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        if($inserted) {
            return true;
        }else{
            return false;
        }

    }

    public function encrypt( $string, $action = 'e' ) {

        $secret_key = 'my_simple_secret_key';
        $secret_iv = 'my_simple_secret_iv';

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

}