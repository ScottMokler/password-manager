<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 09/10/2017
 * Time: 22:09
 */


class User {

    public function __construct()
    {
        // just chucking these in here as time restriction
        $this->database = 'pwmngr';
        $this->dbusername = 'root';
        $this->dbpassword = 'root';

        // connect or die!
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname='. $this->database, $this->dbusername, $this->dbpassword);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }


    public function login($username, $password) {

        // clean all of the things!
        $username = filter_var($username,FILTER_SANITIZE_STRING);
        $password = filter_var($password,FILTER_SANITIZE_STRING);

        //simple sql
        $sql = "SELECT * FROM users WHERE username = '".$username."'";

        // grab the user data and set local var for password comparison
        foreach($this->conn->query($sql) as $row){
            $userpw = $row['password'];
            $uid = $row['id'];
        }

        // verify password
        if(password_verify($password, $row['password'])) {
            // win.rar

            // set session
            $_SESSION['loggedin'] = 1;
            $_SESSION['username'] = $username;
            $_SESSION['uid'] = $uid;

            // redirect to password page
            header("Location: /passwords.php");
            die();
        }else{
            // fail
            $message = 'Sorry, the password you entered is incorrect. Please try again.';
            return $message;
        }

    }

    public function checkValidUser($username) {

        if($username=='') {
            // Clear session as there might be something hanging causing users to end up here.
            unset($_SESSION);
            return false;
        }

        $sql = "SELECT * FROM users WHERE username = '".$username."'";

        // grab the user data and set local var for password comparison
        foreach($this->conn->query($sql) as $row){
            return true;
        }


    }

}