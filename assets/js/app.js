$(document).foundation()

var site = "http://" + window.location.hostname + "/";

$(document).ready(function() {

    $('.pwlink').click(function() {

        var pwid = $(this).attr( "data-pwid" );

        $.ajax({
            url: site + "passwordDetail.php?password=" + pwid,
            context: document.body,
            success:function(data) {
                $('#contentArea').html(data);
            }//end success
        })//end ajax
    });

    $('#delete').click(function() {

        var pwid = $(this).attr( "data-pwid" );
        var url = site + "delete.php?password=" + pwid;

        alert(url);

        // $.ajax({
        //     url: site + "delete.php?password=" + pwid,
        //     context: document.body,
        //     success:function(data) {
        //         $('#contentArea').html(data);
        //     }//end success
        // })//end ajax
    });
});