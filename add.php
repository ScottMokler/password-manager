<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 09/10/2017
 * Time: 22:49
 */

include 'classes/user.php';
include 'classes/storage.php';
session_start();
$user = new User;
$storage = new storage;

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==1) {

    // check user session is valid, if so redirect to password page
    $username = $_SESSION['username'];

    if(!$user->checkValidUser($username)) {
        header("Location: /index.php");
    }

}else{
    header("Location: /index.php");
}

if(isset($_POST['user_name'])) {

    $storage->saveDetails($_POST);
}

?>
<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Password manager</title>
    <link rel="stylesheet" href="assets/css/foundation.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>

<div id="menu">
    <?php include 'inc/menu.php'; ?>
</div>

<div id="sidebar">



</div>
<div id="contentArea" class="addForm">

    <form action="" method="post">
        <div class="row">
            <div class="large-12 columns">
                <label for="sitename">Site name</label>
                <input type="text" name="sitename">
            </div></div>
        <div class="row">
            <div class="large-12 columns">
                <label for="user_name">username</label>
                <input type="text" name="user_name">
            </div></div>
            <div class="row">
                <div class="large-12 columns">
            <label for="password">Password</label>
            <input type="password" name="password">
        </div></div>
                <div class="row">
                    <div class="large-12 columns">
            <label for="notes">notes</label>
            <textarea name="notes"></textarea>
        </div></div>
                    <div class="row">
                        <div class="large-12 columns">
            <input type="submit" class="button">
                        </div>
                    </div>
    </form>

</div>

<script src="assets/js/vendor/jquery.js"></script>
<script src="assets/js/vendor/what-input.js"></script>
<script src="assets/js/vendor/foundation.js"></script>
<script src="assets/js/app.js"></script>
</body>
</html>

