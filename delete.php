<?php
/**
 * Created by PhpStorm.
 * User: scott
* Date: 13/10/2017
* Time: 12:23
*/

include 'classes/user.php';
include 'classes/storage.php';
session_start();
$user = new User;
$storage = new storage;

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==1) {

    // check user session is valid, if so redirect to password page
    $username = $_SESSION['username'];

    if(!$user->checkValidUser($username)) {
        header("Location: /index.php");
    }

}else{
    header("Location: /index.php");
}

$pwid = filter_var($_GET['password'],FILTER_SANITIZE_NUMBER_INT);

if($storage->delete($pwid)) {
    echo "<p>Deleted</p>";
}else{
    echo "<p>There seems to have been a problem deleting this entry</p>";
}