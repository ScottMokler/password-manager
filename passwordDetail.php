<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 12/10/2017
 * Time: 22:21
 */

include 'classes/user.php';
include 'classes/storage.php';
session_start();
$user = new User;
$storage = new storage;

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==1) {

    // check user session is valid, if so redirect to password page
    $username = $_SESSION['username'];

    if(!$user->checkValidUser($username)) {
        header("Location: /index.php");
    }

}else{
    header("Location: /index.php");
}

$passwords = $storage->getPasswordDetail($_SESSION['uid'], $_GET['password']);

if(!$passwords) {
    ?>
        <div class="alert button">
            These aren't the droids you're looking for
        </div>
    <?php
    die();
}

foreach($passwords as $password):

?>



<div class="passwordContent">

    <div class="sitename">
        <span><?php echo($password['site_name']);?></span>
    </div>
    <div class="username">
        <span>Username</span>
        <p><?php echo($password['user_name']);?></p>
    </div>
    <div class="notes">
        <span>Notes</span>
        <p><?php echo($password['notes']);?></p>
    </div>
    <div class="password">
        <span>Password</span>
        <p><?php echo($storage->encrypt($password['password'], 'd'));?></p>
    </div>

</div>

    <div class="row">
        <div class="large-12 columns">
            <a href="" class="button">Update entry</a>
            <a id="delete" data-pwid="<?php echo($password['id']); ?>" class="alert button" ">Delete entry</a>
        </div>
    </div>

<?php endforeach; ?>