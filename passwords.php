<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 09/10/2017
 * Time: 22:49
 */

include 'classes/user.php';
include 'classes/storage.php';
session_start();
$user = new User;
$storage = new storage;

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==1) {

    // check user session is valid, if so redirect to password page
    $username = $_SESSION['username'];

    if(!$user->checkValidUser($username)) {
        header("Location: /index.php");
    }

}else{
    header("Location: /index.php");
}

?>
<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Password manager</title>
    <link rel="stylesheet" href="assets/css/foundation.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>

<div id="menu">
    <?php include 'inc/menu.php'; ?>
</div>

<div id="sidebar">

    <?php
        foreach($storage->fetchListing($_SESSION['username']) as $listing) {

    ?>

        <div class="list_item">

            <a data-pwid="<?php echo($listing['id']); ?>" class="pwlink">
                <?php echo($listing['site_name']); ?>
            </a>

        </div>

    <?php
        }
    ?>

</div>
<div id="contentArea">
    <div class="row">
        <div class="large-12 columns">
            <p>Please select a password from the left menu, alternatively add a new one in the menu up there ^</p>
            <p>
                This is a WIP.
            </p>
            <p>Todo:</p>
            <ul>
                <li>Seperate php/html better</li>
                <li>Update entries</li>
                <li>Delete entries</li>
                <li>User registration</li>
                <li>Grouping/folders</li>
                <li>Make it look good</li>
            </ul>
        </div>
    </div>
</div>

<script src="assets/js/vendor/jquery.js"></script>
<script src="assets/js/vendor/what-input.js"></script>
<script src="assets/js/vendor/foundation.js"></script>
<script src="assets/js/app.js"></script>
</body>
</html>

