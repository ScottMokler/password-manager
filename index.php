<?php
session_start();
include 'classes/user.php';

$message = '';
$user = new User;

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==1) {

    // check user session is valid, if so redirect to password page
    $username = $_SESSION['username'];
    if($user->checkValidUser($username)) {
        header("Location: /passwords.php");
    }

}

if(isset($_POST['username'])) {

    $message = $user->login($_POST['username'], $_POST['password']);

}

?>

<!doctype html>
<html class="no-js" lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PHP Password manager</title>
        <link rel="stylesheet" href="assets/css/foundation.css">
        <link rel="stylesheet" href="assets/css/styles.css">
    </head>

    <body>

        <div class="grid-container">
            <div class="login grid-x grid-margin-x">

                <div class="small-6 cell">

                    <div class="small-12">
                        <h1>Login</h1>
                    </div>
                    <?php
                        if($message!='') {
                    ?>
                        <div class="alert button">
                        <!-- I know this is a button class, but time... life. -->
                            <?php echo($message);?>
                        </div>
                    <?
                        }
                    ?>
                    <div class="small-12">
                        <form action="#" method="post">
                            <div class="large-12 cell">
                                <label>Username</label>
                                <input type="text" name='username' placeholder="Username" />
                            </div>
                            <div class="large-12 cell">
                                <label>Password</label>
                                <input type="password" name='password' placeholder="********" />
                            </div>
                            <div class="large-12 cell">
                                <input type="submit" value="Login">
                            </div>
                        </form>
                    </div>
                    <div class="small-12">
                        <p>
                            <a href="#">Forgot password</a>
                            <a href="#">Register</a>
                        </p>
                    </div>

                </div>
            </div>
        </div>




        <script src="assets/js/vendor/jquery.js"></script>
        <script src="assets/js/vendor/what-input.js"></script>
        <script src="assets/js/vendor/foundation.js"></script>
        <script src="assets/js/app.js"></script>
    </body>
</html>
