# PHP Password manager
Playing around with password manager in php

## Requirements
- MySQL 5.1 or higher
- php7

## Installation
- Create local database called `pwmngr`
- import `db/export.sql` to database
- change credentials in `classes/user.php` and `classes/storage.php` to match database credentials
- ??
- profit

##
Default user data is:
- Username: scott
- Password: test123
 
## Libraries
- Foundation
- Jquery
